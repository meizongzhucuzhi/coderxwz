const {
  createProgramAction,
  createComponentAction,
  createPageAction,
  createStoreAction
} = require('./actions')
module.exports = function (program) {
  //创建项目
  program.command('create <programName> [other...]')
    .description('创建一个项目')
    .action(createProgramAction)
  // 增加组件
  program.command('addcpn <componentName>')
    .description('增加一个组件，例如：selfcli addcpn NavBar [-d src/components]')
    .action((componentName) => {
      const componentDest = program.opts().dest || 'src/components'
      createComponentAction(componentName, componentDest)
    })
    // 增加页面
  program.command('addpage <pageName>')
  .description('增加一个页面，例如：selfcli addpage about [-d src/pages]')
  .action((pageName) => {
    const pageDest = program.opts().dest || 'src/pages'
    createPageAction(pageName, pageDest)
  })
    // 增加vuex
    program.command('addstore <storeName>')
    .description('增加一个页面，例如：selfcli addstore about [-d src/store/modules]')
    .action((storeName) => {
      const storeDest = program.opts().dest || 'src/store/modules'
      createStoreAction(storeName, storeDest)
    })
}
module.exports = function (program) {
  //设置存放目录
  program.option('-d,--dest <dest>', '设置目标文件夹，例如：src/components/common')
  //设置框架
  program.option('-f,--framework <framework>', '请选择框架')
  //设置版本号
  program.version(require('../../package.json').version)
}
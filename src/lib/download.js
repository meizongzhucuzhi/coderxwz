const download = require('download-git-repo')
const ora = require('ora')
const chalk = require('chalk')
module.exports = function (url, projectName) {
  const spinner = ora().start()
  spinner.text = chalk.red.bold('模板正在拉取...');
  download('direct:'+url,projectName,{clone:true},(err)=>{
    if(err){
      spinner.fail('代码下载失败')
      throw err
    }else{
      spinner.succeed('代码下载成功')
      console.log(chalk.blue.bold('Done!'), chalk.bold('you can run:'));
      console.log('cd ' + projectName);
      console.log('npm install ');
      console.log('npm run dev ');
    }
  })
}
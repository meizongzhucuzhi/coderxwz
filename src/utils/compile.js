//将模板编译成对应文件
const ejs = require('ejs')
const path = require('path')
module.exports = function (fileName, data) {
  return new Promise((resolve, reject) => {
    //得到文件的具体路径
    const filePath = path.resolve(__dirname, '../templates/' + fileName)
    //开始编译模板
    ejs.renderFile(filePath, {
      data
    }, {}, (err,result) => {
      if(err){
        console.log(err)
        reject(err)
      }else{
        resolve(result)
      }
    })
  })
}
const fs = require('fs')
const path = require('path')
module.exports = function (filePath, content) {
  //如果目录不存在，则递归创建目录
  if (!fs.existsSync(path.dirname(filePath))) {
    fs.mkdirSync(path.dirname(filePath), {
      recursive: true
    })
  }
  //再读写文件
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, content, (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve('组件创建成功')
      }
    })
  })
}
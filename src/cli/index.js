#!/usr/bin/env node

const {
  program
} = require('commander');
//配置额外选项
const options = require('../lib/options')
options(program)
//自定义命令
const commands = require('../lib/commands')
commands(program)
//解析命令参数
program.parse(process.argv)
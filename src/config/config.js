module.exports = {
  // 可选择的框架
  framwork: ['vue2全家桶', 'vue3全家桶+Vite', 'express', 'koa', 'egg'],
  // 框架对应的下载地址
  foramworkUrl: {
    'vue2全家桶': 'git@gitee.com:meizongzhucuzhi/vue-template.git',
    'vue3全家桶+Vite': 'git@gitee.com:meizongzhucuzhi/vue3-template.git',
    'express': 'git@gitee.com:meizongzhucuzhi/express-template.git',
    'koa': 'git@gitee.com:meizongzhucuzhi/koa-template.git',
    'egg': 'git@gitee.com:meizongzhucuzhi/egg-template.git'
  }
}